
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
                                  ---  PhyML 20120412  ---                                             
                            http://www.atgc-montpellier.fr/phyml                                          
                         Copyright CNRS - Universite Montpellier II                                 
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			SUP35_aln.best.phy
. Data set: 				#1
. Tree topology search : 		NNIs
. Initial tree: 			BioNJ
. Model of nucleotides substitution: 	Custom (012032)
. Number of taxa: 			10
. Log-likelihood: 			-9735.12814
. Unconstrained likelihood: 		-11168.60554
. Parsimony: 				1592
. Tree size: 				1.45857
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		1.732
. Proportion of invariant: 		0.391
. Nucleotides frequencies:
  - f(A)= 0.34198
  - f(C)= 0.19416
  - f(G)= 0.22514
  - f(T)= 0.23872
. GTR relative rate parameters : 
  A <-> C    1.80307
  A <-> G    5.80592
  A <-> T    1.00000
  C <-> G    1.80307
  C <-> T   15.07814
  G <-> T    1.00000

. Instantaneous rate matrix : 
  [A---------C---------G---------T------]
  -0.64060   0.11828   0.44166   0.08066  
   0.20834  -1.56172   0.13716   1.21621  
   0.67088   0.11828  -0.86982   0.08066  
   0.11555   0.98915   0.07607  -1.18077  



. Run ID:				none
. Random seed:				1492806324
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h0m19s (19 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
