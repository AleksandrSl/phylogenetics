Scheme Name       : together
Scheme lnL        : -9744.12695312
Scheme BIC        : 19675.6439053
Number of params  : 24
Number of sites   : 2460
Number of subsets : 1

Subset | Best Model | # sites    | subset id                        | Partition names                                                                                     
1      | TRN+I+G    | 2460       | c2d7458c24b370fbed98a90d4bac9404 | NM_pos1, NM_pos2, NM_pos3, C_pos1, C_pos2, C_pos3                                                   


Scheme Description in PartitionFinder format
Scheme_together = (NM_pos1, NM_pos2, NM_pos3, C_pos1, C_pos2, C_pos3);

Nexus formatted character sets
begin sets;
	charset Subset1 = 1-1159\3 2-1159\3 3-1159\3 1160-2460\3 1161-2460\3 1162-2460\3;
	charpartition PartitionFinder = Group1:Subset1;
end;


Nexus formatted character sets for IQtree
Warning: the models written in the charpartition are just the best model found in this analysis. Not all models are available in IQtree, so you may need to set up specific model lists for your analysis

#nexus
begin sets;
	charset Subset1 = 1-1159\3 2-1159\3 3-1159\3 1160-2460\3 1161-2460\3 1162-2460\3;
	charpartition PartitionFinder = TRN+I+G:Subset1;
end;


RaxML-style partition definitions
Warning: RAxML allows for only a single model of rate heterogeneity in partitioned analyses. I.e. all partitions must be assigned one of three types of model: No heterogeneity (e.g. GTR); +G (e.g. GTR+G); or +I+G (e.g. GTR+I+G). If the best models for your datasetcontain different types of model for different subsets you will need to decide on the best rate heterogeneity model before you run RAxML. If you prefer to do things more rigorously, you can run separate PartitionFinder analyses for each type of rate heterogenetity Then choose the scheme with the lowest AIC/AICc/BIC score. Note that these re-runs will be quick!

DNA, Subset1 = 1-1159\3, 2-1159\3, 3-1159\3, 1160-2460\3, 1161-2460\3, 1162-2460\3


MrBayes block for partition definitions
Warning: MrBayes only allows a relatively small collection of models. If any model in your analysis is not one that is included in MrBayes (e.g. by setting nst = 1, 2, or 6 for DNA sequences; or is not in the available list of protein models for MrBayes)then this MrBayes block will just set that model to nst = 6 for DNA, or 'wag' for Protein. Similarly, the only additional parameters that this MrBayes block will include are +I and +G. Other  parameters, such as +F and +X, are ignored. If you want to use this MrBayes block for your analysis, please make sure to check it carefully before you use it we've done our best to make it accurate, but there may be errors that remain!

begin mrbayes;

	charset Subset1 = 1-1159\3 2-1159\3 3-1159\3 1160-2460\3 1161-2460\3 1162-2460\3;

	partition PartitionFinder = 1:Subset1;
	set partition=PartitionFinder;

	lset applyto=(1) nst=6 rates=invgamma;

end;
