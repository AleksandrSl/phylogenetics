from ete3 import Tree, NodeStyle


def draw_tree(tree_file: str) -> None:
    with open(tree_file, 'r') as in_f:
        tree = Tree(in_f.read(), format=1)  # Newick flexible with internal node names

    for leaf in tree.get_leaves():
        nstyle = NodeStyle()
        nstyle['fgcolor'] = 'red' if ('Agos' in leaf.name or 'Kla' in leaf.name) else 'blue'
        nstyle['hz_line_color'] = 'red' if ('Agos' in leaf.name or 'Kla' in leaf.name) else 'blue'
        leaf.set_style(nstyle)

    tree.show()  # Show full tree
    tree.render('{}.png'.format(tree_file.split('.')[0]), w=183, units='mm')

# draw_tree('Trees/muscle_nj.nwk')
# draw_tree('Trees/muscle_upgma.nwk')
# draw_tree('Trees/SUP35_aln.best.fas.tree')