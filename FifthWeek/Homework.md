# Отчет

*(2 балла) Не забывайте указывать версии использованных программ!*

Версии программ:

|Program |Version       |
|--------|--------------|
|Mafft   |v7.271        |
|Muscle  |v3.8.3       1|
|jmodeltest|2.1.10|
|partitionfinder|2.1.1|
|RAxML|8.2.9|
|PhyML|  20120412|
|Dendroscope|3.5.8|


*(1 балл) Постройте UPGMA-дерево для выравнивания с помощью mafft*
```commandline
mafft --treeout Alignments/SUP35_aln.best.fas > Trees/SUP35_aln.best.fsa
```
Глупый mafft забывает ставить в конце файла";"

*(2 балла) Постройте UPGMA- и NJ-деревья для выравнивания с помощью muscle*
```commandline
muscle -maketree -in Alignments/SUP35_aln.best.fas -out Trees/muscle_upgma.nwk
muscle -MAKEtrEe -in Alignments/SUP35_aln.best.fas -oUT Trees/muscle_nj.nwk -ClUsTer nEiGHborJoiNIng
```

*(4 балла) Отобразите три полученных дерева (картинки должны быть включены в отчёт). Какое(ие) из этих деревьев являе(ю)тся укоренённым? Подтверждают ли они выбор клады Agos + Kla в качестве внешней группы?*

Файлы в папке Trees, SUP35_aln.png - mafft, muscle_upgma.png и muscle_nj.png понятно как получились.
На upgma от muscle обе клады вполне внешние, на upgma от mafft в принципе тоже.
А вот на nj от muscle эти клады каким-то образом затесались в середину,
вроде даже если поменять немного топологию лучше не станет.

На укорененные по внешней группе похожи upgma деревья и от muscle и от mafft.

Картинки почему-то интегрироваться не захотели, оставил ссылки, файлы лежат здесь же в репозитории

[muscle_upgma](https://bitbucket.org/AleksandrSl/phylogenetics/src/032140df12d662b8b1c9bfe1121bee377979fa2a/FifthWeek/Trees/muscle_upgma.png)

[muscle_nj](https://bitbucket.org/AleksandrSl/phylogenetics/src/032140df12d662b8b1c9bfe1121bee377979fa2a/FifthWeek/Trees/muscle_nj.png)

[mafft_upgma](https://bitbucket.org/AleksandrSl/phylogenetics/src/032140df12d662b8b1c9bfe1121bee377979fa2a/FifthWeek/Trees/SUP35_aln.png)

*(3 балла) Запустите jModelTest2 для этого выравнивания (нужно привести либо код для запуска, либо подробное описание действий в GUI). Для нашего упражнения достаточно протестировать 24 модели из 88 (при использовании графической версии нужно указать 3 substitution schemes, best base tree search; в коде — см. мануал). Если есть время, то запустите 88 моделей (11 substitution schemes). Какая схема оказывается лучшей?*

Запустил 88 моделей.
```commandline
java -jar ~/Downloads/jmodeltest-2.1.10/*.jar -d Alignments/SUP35_aln.best.fas -g 4 -BIC -f -i -s 11 > Trees/jmodeltest.log
```
Лучшая модель - TIM3+I+G


*(2 балла) Подготовьте выравнивание для анализа в PartitionFinder (приведите код или опишите действия, которые осуществляли, чтобы получить нужный файл).
Подсказка: посмотрите в мануале, какие форматы выравнивания читает эта программа. Обратите внимание, что в случае sequential формата каждая последовательность должна быть записана в одну строку, а в случае interleaved между блоками выравнивания должны присутствовать пустые строки.*

С помощью R, как делали на паре:
```R
library(ape)
aln <- read.FASTA("SUP35_aln.best.fas")
write.dna(aln, "SUP35_aln.best.phy", format="sequential", colw=10000)
```

*(3 балла) На основе шаблона файла конфигурации из папки с материалами к занятию создайте файл конфигурации, заставляющий PartitionFinder анализировать всю последовательность как единое целое (подсказка: вся необходимая информация есть в шаблоне файла, нужно только снять комментарии у правильных строк). Запустите PartitionFinder с этим файлом конфигурации и подготовленным выравниванием. Приведите код для запуска и файл конфигурации (можно текстом внутри отчёта). Отличаются ли модели, которые подбирают PartitionFinder и jModelTest2 для целой последовательности?*
Конфигурационный файл - FifthWeek/part_finder_whole/partition_finder.cfg
```commandline
python2.7 ~/Downloads/partitionfinder-2.1.1/PartitionFinder.py --force-restart . &> part_finder.log
```
TRN+I+G  - лучшая по мнению partition finder, TIM3+I+G  в нем просто отсутсвует. Выбранная partition finder TrN+I+G - вторая в результатах jmodeltest2

*(3 балла) На основе того же шаблона создайте файл конфигурации, заставляющий PartitionFinder сравнить четыре схемы разбиения:
последовательность целиком (без разбиения);
два отдельных (идущих друг за другом) домена последовательности;
каждая из трёх позиций кодонов по отдельности для всей последовательности;
каждая из трёх позиций кодонов по отдельности в двух отдельных доменах.
Приведите файл конфигурации (можно текстом внутри отчёта). Запустите PartitionFinder с этим файлом конфигурации (уточнение: это один запуск, сравнивающий 4 схемы, а не 4 запуска) и всё тем же подготовленным выравниванием. Какая схема разбиения данных (сколько партиций и каких) оказывается наилучшей?*

Команда запуска точно такая же, только в другой папке.
Конфигурационный файл - FifthWeek/part_finder_four_schemes/partition_finder.cfg
Лучшая схема separate(4), когда все позиции кодонов и оба домена рассматриваются отдельно.
Причем для каждой части судя по всему выбралась своя модель.

|Subset | Best Model | # sites    | subset id                        | Partition names|
|-------|------------|------------|----------------------------------|----------------|
|1      | HKY+G      | 387        | 9d66431223fe8070284aa3bd9a451afb | NM_pos1        |
|2      | HKY+I      | 386        | 59e6f334800aef29b765a9d967734816 | NM_pos2        |
|3      | TRNEF+G    | 386        | c39f0f9d2e76889160c0f434fcee270e | NM_pos3        |
|4      | GTR        | 434        | cfff352263bab294968aab40f518285a | C_pos1         |
|5      | HKY+I      | 434        | b2241e69f659a9a0d916913e0a70d465 | C_pos2         |
|6      | TRN+I      | 433        | 6c86ad5f1e5c39fed766d3f586f6f19e | C_pos3         |



*(3 балла) Постройте в RAxML ML-дерево (500 bootstrap replicates) на основе целой последовательности и модели GTRCAT. Приведите код для запуска программы.*

```commandline
~/Downloads/partitionfinder-2.1.1/programs/raxml.linux -m GTRCAT -n GTRCAT_ML_tree -s /home/aleksandrsl/Projects/phylogenetics/FifthWeek/raxml/SUP35_aln.best.fas -p 222 -N 500 -b 222
~/Downloads/partitionfinder-2.1.1/programs/raxml.linux -m GTRCAT -n bipart -s /home/aleksandrsl/Projects/phylogenetics/FifthWeek/raxml/SUP35_aln.best.fas -p 222 -z RAxML_bootstrap.GTRCAT_ML_tree
~/Downloads/partitionfinder-2.1.1/programs/raxml.linux -m GTRCAT -n bipart_bootstrap.nwk -s /home/aleksandrsl/Projects/phylogenetics/FifthWeek/raxml/SUP35_aln.best.fas -p 222 -z RAxML_bootstrap.GTRCAT_ML_tree -f b -t RAxML_bestTree.bipart
```


*(3 балла) Постройте в RAxML ML-дерево (500 bootstrap replicates) на основе последовательности, разбитой по наилучшей (согласно PartitionFinder) схеме, и отдельной модели GTRCAT для каждой партиции. Приведите код для запуска программы (подсказка: Вам понадобится отдельный файл со схемой разбиения, его можно подготовить самостоятельно или взять из папки с материалами к занятию).*
```commandline
~/Downloads/partitionfinder-2.1.1/programs/raxml.linux -m GTRCAT -n GTRCAT_ML_tree -s /home/aleksandrsl/Projects/phylogenetics/FifthWeek/raxml/SUP35_aln.best.fas -p 222 -N 500 -b 222 -q partitions_for_raxml.txt
~/Downloads/partitionfinder-2.1.1/programs/raxml.linux -m GTRCAT -n bipart -s /home/aleksandrsl/Projects/phylogenetics/FifthWeek/raxml/SUP35_aln.best.fas -p 222 -z RAxML_bootstrap.GTRCAT_ML_tree
~/Downloads/partitionfinder-2.1.1/programs/raxml.linux -m GTRCAT -n bipart_bootstrap.nwk -s /home/aleksandrsl/Projects/phylogenetics/FifthWeek/raxml/SUP35_aln.best.fas -p 222 -z RAxML_bootstrap.GTRCAT_ML_tree -f b -t RAxML_bestTree.bipart
```


*(6 баллов) Укорените деревья, полученные в пп. 8 и 9, и “схлопните” (переведите в политомичный вид) клады, поддержка бутстрепа которых менее 70%. Отобразите полученные деревья с масштабной линейкой и указанием поддержки бутстрепа (способ создания рисунка можно выбрать любой). Картинку нужно включить в отчёт. На основании построенных деревьев опишите родственные отношения между таксонами S. cerevisiae, S. boulardii и S. paradoxus. Что происходит с положением внешней группы, если использовать при построении дерева модель с разбиением последовательности на несколько партиций? (Бонусный вопрос) Какой вывод можно из этого сделать?*

Укоренял и "схлопывал" с помощью dendroscope.
Edit -> Contract Low Support Edges -> 70
Edit -> Midpoint Root
Линейку он мне рисовать не захотел(пробовал View -> Show Scale Bar)

Возможно что-то где-то пошло не так, но у меня оба дерева похожи, за исключением поддержки одного узла. Таксоны Ago и Kla в обоих случаях хорошо выделяются как внешняя группа.
S.cerevisiae и S.boulardii объединяются в одну группу сестринскую по отношению к S.paradoxus.

[simple_scheme](https://bitbucket.org/AleksandrSl/phylogenetics/src/43e344912e723ba59c46b4dca73f15d7a63d19e7/FifthWeek/raxml/simple_scheme/RAxML_bipartitions.bipart_bootstrap.bmp)
[elaborate_scheme](https://bitbucket.org/AleksandrSl/phylogenetics/src/43e344912e723ba59c46b4dca73f15d7a63d19e7/FifthWeek/raxml/elaborate_scheme/RAxML_bipartitions.bipart_bootstrap.bmp)

*(Дополнительный вопрос, бонус 4 балла) Из двух подобранных моделей эволюции для целой последовательности (лучших по результатам работы jModelTest2 либо PartitionFinder) выберите любую, и постройте на основе неё и целой последовательности ML-дерево в phyml (500 bootstrap replicates). Приведите код для запуска программы.
Предупреждение: обычная версия phyml считается на одном процессоре, что может занять значительное время (для 500 бутстрепов более 1 часа). Параллельная работа на нескольких процессорах возможна, но запускается сложнее, чем у RAxML; подробности см. в мануале.
Укорените полученное дерево, “схлопните” клады, поддержка бутстрепа которых менее 70%, и отобразите деревья с масштабной линейкой и указанием поддержки бутстрепа. Картинку нужно включить в отчёт. Отличаются ли деревья, построенные по целой последовательности с помощью phyML и RAxML?*
```commandline
# В интернете написано, что 012032 соответствует TIM3
~/Downloads/partitionfinder-2.1.1/programs/phyml.linux -i ~/Projects/phylogenetics/FifthWeek/PartFinder/SUP35_aln.best.phy -q -b 500 -m 012032 -v e -a e
# Результат создается в той же папке, что и исходный файл
```
Для укоренения и схлопывания воспользовался все тем же Dendroscope. Только почему то максимальный bootstrap вышел 500, видимо по количеству самих бутстрэпов. Поэтому в качестве порога для "схлопывания" взял 350.
[phyml_TIM3](https://bitbucket.org/AleksandrSl/phylogenetics/src/43e344912e723ba59c46b4dca73f15d7a63d19e7/FifthWeek/phyml/SUP35_aln.best.phy_phyml_tree.bmp?at=master&fileviewer=file-view-default)

Большой разницы между деревьями я не вижу, единственно отличие на дереве phyml
SUP35_Scer_74-D694_GCA_001578265.1 и SUP35_Sbou_unique28_CM003560 оказываются в одной группе, а является сестринской к ним группой SUP35_Scer_beer078_CM005938.
Это немного странно, так как два штамма одного вида дальше друг от друга, чем штаммы близких видов.