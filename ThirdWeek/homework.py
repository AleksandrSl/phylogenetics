from Bio import Entrez

Entrez.email = 'Alex121994@mail.ru'
# Get all entries by gene name
handle = Entrez.esearch(db='nucleotide', term='cftr AND human[ORGN]', retmode="xml")
record = Entrez.read(handle)
handle.close()
info = {}
for id in record['IdList']:
    print(id)
    # Get summary information about gene
    with Entrez.esummary(db = 'nucleotide', id=id, retmode='xml') as in_f:
        summary = Entrez.parse(in_f)
        for record in summary:
            info[record['Id']] = (record['Caption'], record['Length'])
        with open('{}.fasta'.format(record['Caption']), 'w') as out_f:
            # Get sequence and write it to file
            out_f.write(Entrez.efetch(db="nucleotide", id=id, rettype="fasta", retmode="text").read())
print(info)

sequences = set()
# Get all links from article to sequences
with Entrez.elink(db='nucleotide', dbfrom='pubmed', id=12890024) as in_f:
    for el in Entrez.parse(in_f):
        for linkgroup in el['LinkSetDb']:
            for id in linkgroup['Link']:
                sequences.add(Entrez.efetch(db="nucleotide", id=id['Id'], rettype="fasta", retmode="text").read())
print(len(sequences))
