# Отчет

*(1 балл) Не забудьте указать версии программ (mrbayes и любых других использованных в задании)*

Версии программ:

|Program |Version       |
|--------|--------------|
|RAxML|8.2.9|
|Dendroscope|3.5.8|
|MrBayes|v3.2.6|
|R|3.3.1 (2016-06-21)|
|ape|4.1|
|seqinr|3.3-6|


1. *(2 балла) переведите выравнивание в формат nexus, который прочитает mrbayes (нужно написать код или описать выполненные действия)*

Перевод как на занятии, с помощью R:
```R
library(ape)
library(seqinr)
aln <- read.fasta("SUP35_aln.best.fas")
write.nexus.data(aln, "SUP35_aln.best.nex")
```

2. *(1 балл) Запустите программу в интерактивном режиме*
3. *(1 балл) Прочитайте файл SUP35_aln.nexus*
4. *(2 балла) Установите модель GTR+I+G. Кстати, как вызвать справку по моделям*
5. *(1 балл) Запустите 100 тыс. поколений марковской цепи. Сошлась ли цепь (standard deviation of split frequences < 0.01)? Если нет, сколько пришлось добавить поколений, чтобы цепь сошлась?*
6. *(1 балл) Суммируйте параметры модели*
7. *(2 балла) Суммируйте деревья*
```MrBayes
mb

Execute Alignments/SUP35_aln.best.nex  # Прочитать файл
Lset nst=6 rates=invgamma  # Установить модель GTR+I+G
#GTR - 6 типов замен 
Mcmc ngen=1000000  # Запустить марковскую цепь на 10^6 поколений
Sump  # Суммировать парметры модели
# Sumt # В формате nexus
Sumt conformat=simple # Nexus файл с деревом в формате nwk
```
Запустил по умолчанию с 1000000 поколений. standard deviation of split frequencies < 0.01 было на 95000 поколении, потом то больше то меньше. После 1000000 поколений вышло 0.005418


8. *(4 балла) Нарисуйте рядом деревья, полученные с помощью RAxML и MrBayes. Рисунок нужно включить в отчёт и сопроводить подписью, позволяющей понять, где какое дерево и что означают числа около узлов. При каком отсечении по значениям поддержки bootstrap / апостериорной вероятности топология этих деревьев будет совпадать?* 

Raxml tree:
![raxml](RAxML.png)
Цифры в узлах - поддержка bootstrap

MrBayes tree:
![mrbayes](MrBayes.png)
Цифры в узлах - clade credibility (Each clade within the tree is given a score based on the fraction of times that it appears in the set of sampled posterior trees, and the product of these scores are taken as the tree's score.)

В общем-то топология деревьев и так похожа, за исключением того, что Raxml выделяет в отдельную группу *S.cerevisiae* и *S.boulardii*.
Если в дереве от Raxml схлопнуть ветви с поддержкой менее 65, а в дереве от MrBayes с clade credibility менее 0.75, то топология деревьев совпадет

Raxml tree collapsed by 65:
![raxml_collapsed](RAxML_collapsed.png)

MrBayes tree collapsed by 0.75:
![mrbayes_collapsed](MrBayes_collapsed.png)
