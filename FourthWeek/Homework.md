# Отчет

*(2 балла) Не забывайте указывать версии использованных программ!*

Версии программ:

|Program |Version       |
|--------|--------------|
|ClustalW|v2.1          |
|Mafft   |v7.271        |
|Kalign  |v2.04         |
|Muscle  |v3.8.3       1|
|Prank   |v.150803      |
|T Coffee|v11.00.8cbe486|

*(1 балл) Какие последовательности использовали для анализа: тип биополимера, организм, ген (напоминаю, они в папке с дополнительными материалами к занятию).*

Для анализа использовали последовательности ДНК гена SUP35 дрожжей рода Saccharomyces плюс еще два вида грибов, наверное, Kluyveromyces и кто-то еще.

*(3 балла) Код для запуска 6 возможных алгоритмов выравнивания (clustalw, muscle, mafft, kalign, tcoffee, prank) для 10 последовательностей ДНК (SUP35_10seqs.fa) + вариации параметров, если они были.*
```commandline
# In clustal format by default
time clustalw Sequences/SUP35_10seqs.fa -OUTFILE=Alignments/SUP35_10seqs_clustalw.aln
# Also creeates .dnd file near the input file

time muscle -in Sequences/SUP35_10seqs.fa -out Alignments/SUP35_10seqs_muscle.fa

time mafft --auto Sequences/SUP35_10seqs.fa > Alignments/SUP35_10seqs_mafft.fa

time kalign < Sequences/SUP35_10seqs.fa > Alignments/SUP35_10seqs_kalign.fa

time t_coffee -in Sequences/SUP35_10seqs.fa > Alignments/SUP35_10seqs_t_coffee.aln

time prank -d=Sequences/SUP35_10seqs.fa -o=Alignments/SUP35_10seqs_prank.fa
# 5 iterations by default
```


*(3 балла) Сравнительная таблица со временем работы* и комментариями по поводу качества выравнивания ДНК для указанных выше алгоритмов**. Какой алгоритм лучше использовать?*

Самый медленный t_coffee

|Tool     | Time      |
|---------|:---------:|
|ClustalW | 0m4.788s  |
|Muscle   | 0m4.539s  |
|Mafft    | 0m3.635s  |
|Kalign   | 0m0.515s  |
|T Coffee | 1m10.881s |
|Prank    | 0m9.287s  |


*(2 балла) Что не так с выравниванием SUP35_10seqs_strange_aln.fa и как это исправить?*

Последовательность SUP35_Agos_ATCC_10895_NM_211584 записана в виде
обратно комплименатрной, нужно соответсвенно еще раз сделать ее обратно
комплиментарной, чтобы все было хорошо. Например, с помощью Ugene.

*(1 балл) Как получили последовательности аминокислот (транслировали)?*
Можно в Ugene: `Export -> Amino translation` или `transeq  Sequences/SUP35_10seqs.fa -out Sequences/SUP35_10seqs.faa`

*(2 балла) Перевод в аминокислотные последовательности, какие проблемы возникают.*

С указанными выше командами проблем не было. Возможно проблемы заключаются в том, что мы не знаем с какого места начинать трансляцию, какая рамка считывания.


*(3 балла) Команды для запуска 6 возможных вариантов выравнивания для 10 белковых последовательностей + вариации параметров, если они были.*
```
time clustalw Sequences/SUP35_10seqs.faa -OUTFILE=Alignments/SUP35_10seqs_aa_clustalw.aln

time muscle -in Sequences/SUP35_10seqs.faa -out Alignments/SUP35_10seqs_muscle.faa

time mafft --auto Sequences/SUP35_10seqs.faa > Alignments/SUP35_10seqs_mafft.faa

time kalign < Sequences/SUP35_10seqs.faa > Alignments/SUP35_10seqs_kalign.faa

time t_coffee -in Sequences/SUP35_10seqs.faa > Alignments/SUP35_10seqs_aa_t_coffee.aln

time prank -d=Sequences/SUP35_10seqs.faa -o=Alignments/SUP35_10seqs_prank.faa
```

*(3 балла) Сравнительная таблица со временем работы и комментариями по поводу качества выравнивания белков. Какой алгоритм лучше использовать?*

|Tool     | Time      |
|---------|:---------:|
|ClustalW | 0m0.757s  |
|Muscle   | 0m0.372s  |
|Mafft    | 0m0.718s  |
|Kalign   | 0m0.072s  |
|T Coffee | 0m0.285s  |
|Prank    | 0m17.591s |

Все стало работать быстрее, аминокислотные последовательности как минимум короче, t coffe исправился и стал быстрым.
Только prank стал работать дольше.

*(1 балл) Команды для запуска 6 возможных вариантов выравнивания (см. п. 2) для 250 последовательностей ДНК.*
```
# In clustal format by default
time clustalw Sequences/SUP35_250seqs.fsa -OUTFILE=Alignments/SUP35_250seqs_clustalw.aln
# Also creeates .dnd file near the input file

time muscle -in Sequences/SUP35_250seqs.fsa -out Alignments/SUP35_250seqs_muscle.fa

time mafft --auto Sequences/SUP35_250seqs.fsa > Alignments/SUP35_250seqs_mafft.fa

time kalign < Sequences/SUP35_250seqs.fsa > Alignments/SUP35_250seqs_kalign.fa

time t_coffee -in Sequences/SUP35_250seqs.fsa > Alignments/SUP35_250seqs_t_coffee.aln

time prank -d=Sequences/SUP35_250seqs.fsa -o=Alignments/SUP35_250seqs_prank.fa
# 5 iterations by default

```


*(3 балла) Сравнительная таблица со временем работы и комментариями по поводу качества выравнивания 250 последовательностей ДНК (SUP35_250seqs.fa). Изменился ли наш выбор алгоритма?*

|Tool     | Time      |
|---------|:---------:|
|ClustalW | 44m54.560s|
|Muscle   |3m16.359s  |
|Mafft    |0m31.779s  |
|Kalign   |0m28.735s  |
|T Coffee |>40m       |
|Prank    |11m4.257s  |


Не стал бы трогать T Coffee, так как при его запуске зависает все и работает долго.

*(2 балла) Как добавить к выравниванию 250 нуклеотидных последовательностей ещё одну (SUP35_1addseq.fsa) с помощью mafft и muscle?*
```
muscle -profile -in1 Alignments/SUP35_250seqs_muscle.fa -in2 Sequences/SUP35_1addseq.fsa -out Alignments/SUP35_251seqs_muscle.fa
mafft --add Sequences/SUP35_1addseq.fsa --reorder Alignments/SUP35_250seqs_mafft.fa > Alignments/SUP35_251seqs_mafft.fa
```

*(2 балла) Как добавить к выравниванию 250 нуклеотидных последовательностей ещё две (SUP35_2addseqs.fsa), предварительно выровняв их, с помощью mafft и muscle?*
```
muscle -in Sequences/SUP35_2addseqs.fsa -out Alignments/SUP35_2addseqs_muscle.fa
muscle -profile -in1 Alignments/SUP35_250seqs_muscle.fa -in2 Alignments/SUP35_2addseqs_muscle.fa -out Alignments/SUP35_250seqs_muscle.fa

mafft --add Sequences/SUP35_2addseqs.fsa --reorder Alignments/SUP35_250seqs_mafft.fa > Alignments/SUP35_252seqs_mafft.fa
```


*(2 балла) Как запустить Gblocks (для выравнивания 10 и 250 последовательностей)? Что остаётся от выравнивания после запуска Gblocks со строгими и нестрогими параметрами (указать конкретные параметры)***?*
```
# Минимальая команда
Gblocks Alignments/SUP35_10seqs_muscle.fa -t=d
# -t=c (выравнивание по кодонам)
# будем сохранять/убирать целые кодоны
Gblocks Alignments/SUP35_10seqs_muscle.fa -t=d -b1=6, b4=5, b5=h
b1 - необходимое количество последовательностей для позиции, чтобы продолжить с ней работу
b2 - то же самое, но для фланкирующих позиций
b4 - максимальное число неконсервативных позиций подряд
b5 - сколько мы допускаем гэпов (n/h/a)
```
