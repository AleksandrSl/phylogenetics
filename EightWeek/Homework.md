# Отчет

*(1 балл) Не забудьте указать версии программ

Версии программ:

|Program |Version       |
|--------|--------------|
|R|3.3.1 (2016-06-21)|
|Pophelper library|2.2.0|
|Structure|Version 2.3.4|


Structure занимается кластеризацией аллелей.
Первая строка - название локусов. Все остальные строки в след формате:
название образца - код локуса.
По 2 строки на образец - потому что медведи диплоидные.

(2 балла) Скопируйте из папки structure файлы mainparams и extraparams и измените 
необходимые переменные (по крайней мере INFILE, NUMINDS, NUMLOCI и EXTRACOLS) 
в файле mainparams в соответствии с информацией из файла 1216424s2_structure.txt. 
Приведите (можно в тексте отчёта) изменённые строки.
```
#define INFILE  1216424s2_structure.txt  // (str) name of input data file
#define OUTFILE  bears_structure.txt  //(str) name of output data file
#define EXTRACOLS 1     // (int) Number of additional columns of data before the genotype data start.
#define NUMINDS    45    // (int) number of diploid individuals in data file
#define NUMLOCI    14   // (int) number of loci in data file
```

(2 балла) Запустите Structure в режиме поиска 3, 4 или 5 кластеров (K). Приведите код для запуска (или нужные строки файлов, если этот параметр также изменяли в файле).

```
for i in 3 4 5; do ../../console/structure -o bears_$i -K $i; done
```

(5 баллов) Визуализируйте результат с помощью собственного кода, distruct,
 pophelper или любого другого инструмента и приведите код или подробное описание действий.
 Должен получиться рисунок с 3 панелями, причём на них должна быть совместимая цветовая схема, а также подписи групп
 (видов) или каждого образца. Какая из панелей соответствует рисунку 2 из статьи? Подтверждают ли эти данные вывод об
 «отдельности» белых медведей от бурых, сделанный авторами статьи?

Комманда запуска:

```R
library(pophelper)

bears <- readQ(c('bears_3_f', 'bears_4_f', 'bears_5_f'))
grplabels <- data.frame(species = c(rep("Black Bear", 7), rep("Polar Bear", 18), rep("Brown Bear", 19), rep("Giant Panda", 1)), stringsAsFactors = F)

plotQ(bears, showindlab = F, showlegend = F, imgoutput = 'join',
      clustercol = c('#79d279', '#80bfff', '#00ff80','#99ffd6', '#c4ff4d'),
      showdiv = T, grplab = grplabels, grplabangle = 70,
      grplabheight = 2, grplabpos = 0.85, linepos = 0.9, barsize=0.8,
      splab = c("Bears, K = 3", "Bears, K = 4", "Bears, K = 5"))

```


![clusters](data/clusters.png)

Я долго вглядывался в график, но так и не нашел сходства. Но я прочитал,
в статье, что у них график для 5 кластеров и вроде как полярные медведи,
что у авторов статьи, что на моем графике ни с кем не смешиваются, а значит их можно считать отдельными от бурых.