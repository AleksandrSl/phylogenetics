from Bio import Phylo
# import networkx
import pylab
import requests
# import graphviz
from ete3 import Tree, NodeStyle
import random

"""
Make clear conda virtualenv with python3.4(for ete3 dependencies)
conda install -c etetoolkit ete3 ete3_external_apps
Networkx==1.9 (To work with graphviz)
pip install graphviz (conda installs it, but python doesn't see it)
easy_install pygraphviz
"""

tree = requests.get("https://www.jasondavies.com/tree-of-life/life.txt").content.decode()

with open('tree.nwk', 'w') as out_f:
    out_f.write(tree)

tree = Phylo.read('tree.nwk', 'newick')
Phylo.draw_ascii(tree)
Phylo.draw(tree, do_show=False)
pylab.savefig('tree_of_life.svg', format='svg', bbox_inches='tight', dpi=800)
pylab.savefig('tree_of_life.png', format='png', bbox_inches='tight', dpi=800)


Phylo.write(tree, 'tree_of_life.xml', 'phyloxml')
Phylo.draw_graphviz(tree, prog='twopi', width=1, node_size=2, font_size=6)
pylab.show()  # Display the tree


with open('tree.nwk', 'r') as in_f:
    tree = Tree(in_f.read(), format=1)  # Newick flexible with internal node names

random_leaves = random.sample(tree.get_leaf_names(), 42)

colors = ("red", "blue", "yellow", "green", "black")
for leaf in tree.get_leaves():
    nstyle = NodeStyle()
    nstyle["fgcolor"] = random.choice(colors)
    nstyle["size"] = random.randint(10, 20)
    nstyle["hz_line_color"] = colors[leaf.name.count('bac')]
    leaf.set_style(nstyle)
tree.show()  # Show full tree
tree.prune(random_leaves)  # Erase all leaves save for random leaves
tree.render("tree_of_life_ete3.png", w=183, units="mm")

